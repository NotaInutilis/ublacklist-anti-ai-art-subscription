# ublacklist-anti-ai-art-subscription

uBlacklist subscription list that attempts to remove GenAI art from image search results.

The original list has been curated by @cutabello and @nebulousharmony on [Tumblr](https://www.tumblr.com/cutabello/725569613868548096/i-made-a-list-of-as-many-ai-sites-i-could-find-to?source=share).
Thank you very much!

I added some more sites which I found on several other similar projects around the internet.
The goal of this list is to be comprehensive and mostly complete.

## Usage

There are two lists you can choose from. `list.txt` and `nuclear-list.txt`. They are mostly
identical, but the nuclear option includes sites which don't exclusively host AI images, e.g.
Pinterest and DeviantArt (RIP, you used to be awesome). Using this list will also block all
legitimate art that's hosted there. If this is too aggressive for you, choose the normal list.

In order to use it, either copy the contents of either file into uBlocklist, or add one of
the following two links as a subscription. If you subscribe, updates will be fetched
automatically.

### Normal List
```
https://codeberg.org/ranmaru22/ublacklist-anti-ai-art-subscription/raw/branch/main/list.txt
```

### Nuclear List
```
https://codeberg.org/ranmaru22/ublacklist-anti-ai-art-subscription/raw/branch/main/nuclear-list.txt
```

## Contribution

PRs which add more sites are very welcome. Or just open a ticket if you find a site which isn't
covered yet and I will add it.
